/**
 *
 * @author jackgavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Recurssive {
    
    // Initialize the main method
    public static void main(String[] args){
        testPhoneNumber();
    }
    
    // Recursive method
    public static boolean testPhoneNumber(){
                
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Please enter a valid phone number (xxx-xxx-xxxx): ");
        String userInput = input.next();
        
        if(userInput.matches("(\\d{3})(-)(\\d{3})(-)(\\d{4})")){
            return true;
        }else{
            System.out.println("Sorry! That's an invalid phone number. Try Again.");
            return testPhoneNumber();
        }
    }
}
