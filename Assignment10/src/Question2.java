/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Create rectangle
        MyRectangle2D rect = new MyRectangle2D(2,2,5.5,4.9);
        System.out.println("Rectangle has an area of "+rect.getArea());
        System.out.println("Rectangle has perimeter of "+rect.getPerimeter());
        if(rect.contains(3,3)){
            System.out.println("Rectangle contains point 3,3");
        }else{
            System.out.println("Rectangle does not contain point 3,3");
        }
    }
    
    public static class MyRectangle2D{
        
        // Initialize datafields
        private double x;
        private double y;
        private double width;
        private double height;
        
        // Create constructors
        MyRectangle2D(){
            this.x = 1;
            this.y = 1;
            this.width = 1;
            this.height = 1;
        }
        
        MyRectangle2D(double x,double y,double width,double height){
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        
        // Class methods
        void setCenter(double x,double y){
            this.x = x;
            this.y = y;
        }
        double[] getCenter(){
            double[] data = {x,y};
            return data;
        }
        void setWidth(int width){
            this.width = width;
        }
        double getWidth(){
            return width;
        }
        void setHeight(int height){
            this.height = height;
        }
        double getHeight(){
            return height;
        }
        double getArea(){
            return width*height;
        }
        double getPerimeter(){
            return (width*2)+(height*2);
        }
        boolean contains(double cx,double cy){
            double absX = Math.abs(x);
            double absY = Math.abs(y);
            double absXPlus = absX + width;
            double absYPlus = absY + height;
            double absCX = Math.abs(cx);
            double absCY = Math.abs(cy);
            return (absCX<absXPlus)&&(absCY<absYPlus);
        }
    }
}
