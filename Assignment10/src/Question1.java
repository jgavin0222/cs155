/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question1 {
    
    // Inititalize the main method
    public static void main(String[] args){
        
        // Create time objects
        Time t1 = new Time();
        Time t2 = new Time(555550000);
        Time t3 = new Time(5,23,55);
        
        // Display output
        System.out.println("Time 1: "+t1.getHour()+":"+t1.getMinute()+":"+t1.getSecond());
        System.out.println("Time 2: "+t2.getHour()+":"+t2.getMinute()+":"+t2.getSecond());
        System.out.println("Time 3: "+t3.getHour()+":"+t3.getMinute()+":"+t3.getSecond());
    }
    
    public static class Time{
        
        // Initialize data fields
        private int hour;
        private int minute;
        private int second;
        
        // Create constructors
        Time(){
            double currentTime = System.currentTimeMillis();
            int[] parsed = format(currentTime);
            this.hour = parsed[0];
            this.minute = parsed[1];
            this.second = parsed[2];
        }
        Time(int hour,int minute,int second){
            this.hour=hour;
            this.minute=minute;
            this.second=second;
        }
        Time(long elapsedTime){
            int[] parsed = format(elapsedTime);
            this.hour = parsed[0];
            this.minute = parsed[1];
            this.second = parsed[2];
        }
        
        // Class methods
        double getHour(){
            return hour;
        }
        double getMinute(){
            return minute;
        }
        double getSecond(){
            return second;
        }
        void setHour(int hour){
            this.hour = hour;
        }
        void setMinute(int minute){
            this.minute = minute;
        }
        void setSecond(int second){
            this.second = second;
        }
        void setTime(long elapsedTime){
            int[] parsed = format(elapsedTime);
            this.hour = parsed[0];
            this.minute = parsed[1];
            this.second = parsed[2];
        }
    }
    
    public static int[] format(double toParse){
        double totalSeconds = toParse/1000;
        int hours = (int)(totalSeconds / 3600);
        totalSeconds -= hours * 3600;
        int minutes = (int)(totalSeconds / 60);
        totalSeconds -= minutes * 60;
        int hoursMinusDays = hours % 24;
        int[] allign = {hoursMinusDays,minutes,(int)totalSeconds};
        return allign;
    }
}

