/**
 *
 * @author JackGavin
 */

// Initilize the class
public class Question1 {
    
    // Initilize the main method
    public static void main(String[] args){
        
        //Start output of the table
        System.out.println("a   b  pow(a,b)");
        
        // Calculations
        calculate(1,2);
        calculate(2,3);
        calculate(3,4);
        calculate(4,5);
        
    }
    
    private static void calculate(int A,int B){
        int power = (int) Math.pow(A,B);
        System.out.println(A+"  "+B+"   "+power);
    }
}
