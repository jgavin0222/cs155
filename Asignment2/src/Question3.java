/**
 *
 * @author JackGavin
 */

import java.util.Scanner;

// Initialize the class
public class Question3 {
    
    //Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner scanner = new Scanner(System.in);
        
        // User input and calculate
        System.out.print("Enter a value for feet: ");
        double userFeet = scanner.nextDouble();
        double meters = userFeet * 0.305;
        
        // Output the result
        System.out.println(userFeet+" is "+meters+" meters.");
        
    }
}
