/**
 *
 * @author JackGavin
 */

// Pull in Decimal Format and Scanner
import java.util.Scanner;
import java.text.DecimalFormat;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call DecimalFormats and Scanners constructor methods
        Scanner scanner = new Scanner(System.in);
        DecimalFormat decimal = new DecimalFormat("#.####");
        
        // Get user input
        System.out.print("v0: ");
        double v0 = scanner.nextDouble();
        System.out.print("v1: ");
        double v1 = scanner.nextDouble();
        System.out.print("time: ");
        double time = scanner.nextDouble();
        
        // Calculate and format
        double acceleration = (v1 - v0)/time;
        double accelFormat = Double.valueOf(decimal.format(acceleration));
        
        // Output the acceleration
        System.out.println("The average acceleration is "+accelFormat);
        
    }
}
