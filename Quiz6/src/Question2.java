/**
 *
 * @author JackGavin
 */
// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter a string: ");
        String userInput = input.nextLine();
        
        // Get user character
        System.out.print("Enter a character: ");
        String userCharString = input.next();
        char userChar = userCharString.charAt(0);
        
        // Get instances of value
        int countInstances = count(userInput,userChar);
        System.out.println("The character "+userChar+" appears "+countInstances+" times in the string: "+userInput);
        
    }
    
    // Counter method (Heart of the program)
    public static int count (String str, char a){
        // Start instances @ 0
        int instances = 0;
        // Loop through the users input
        for(int i=0;i<str.length();i++){
            if(Character.toLowerCase(str.charAt(i))==Character.toLowerCase(a)){
                instances++;
            }
        }
        // Return how many instances the character appeared
        return instances;
    }
}
