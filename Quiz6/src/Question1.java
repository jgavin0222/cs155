/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get the users input
        System.out.print("Enter three values: ");
        double userNumber1 = input.nextDouble();
        double userNumber2 = input.nextDouble();
        double userNumber3 = input.nextDouble();
        
        // Call the display method
        displaySortedNumbers(userNumber1,userNumber2,userNumber3);
        
    }
    
    // Display numbers method
    public static void displaySortedNumbers (double num1, double num2, double num3){
        
        // Check which number is the smallest
        if((num1<=num2)&&(num1<=num3)){
            // If one is the smallest print it first
            System.out.print(num1+" ");
            // Check which number is smaller, then print sequentially
            if(num2<=num3){
                System.out.println(num2+" "+num3);
            }else{
                System.out.println(num3+" "+num2);
            }
        }else if((num2<=num1)&&(num2<=num3)){
            // If two is the smallest print it first
            System.out.print(num2+" ");
            // Check which number is smaller, then print sequentially
            if(num1<=num3){
                System.out.println(num1+" "+num3);
            }else{
                System.out.println(num3+" "+num1);
            }
        }else if((num3<=num1)&&(num3<=num2)){
            // If three is the smallest print it first
            System.out.print(num3+" ");
            // Check which number is smaller, then print sequentially
            if(num1<=num2){
                System.out.println(num1+" "+num2);
            }else{
                System.out.println(num2+" "+num1);
            }
        }else{
            // If all three numbers are the same value, print 1 2 then 3
            System.out.println(num1+" "+num2+" "+num3);
        }
    }
}
