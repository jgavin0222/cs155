/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class ArrayPush {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Please enter integers separated by a space: ");
        String line = input.nextLine();
        System.out.print("Value to push: ");
        int pushValue=input.nextInt();
        
        // Parse user line
        String[] array = line.split(" ");
        int[] intArray = new int[array.length];
        for(int i=0;i<array.length;i++){
            intArray[i]=Integer.parseInt(array[i]);
        }
        
        // Array logic
        System.out.println("Array before pushing");
        for(int i=0;i<intArray.length;i++){
            System.out.print(intArray[i]+" ");
        }
        System.out.println();
        intArray = push(intArray,pushValue);
        System.out.println("Array after pushing");
        for(int i=0;i<intArray.length;i++){
            System.out.print(intArray[i]+" ");
        }
        System.out.println();
    }
    
    // Push method
    private static int[] push(int[] a,int b){
        int[] c=new int[a.length+1];
        for(int i=0;i<a.length;i++){
            c[i]=a[i];
        }
        c[a.length]=b;
        return c;
    }
}
