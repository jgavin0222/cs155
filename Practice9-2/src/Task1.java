/**
 *
 * @author jackgavin
 */

// Initialize the class
public class Task1 {
    
    // Intialize the main method
    public static void main(String[] args){
        
        // Create a new student
        Student mystudent = new Student();
        
        // Set student data
        mystudent.setName("Test Student");
        mystudent.setAddress("456 Test St.");
        mystudent.setAge(10);
        mystudent.setGpa(3.53);
        
        // Get student data
        String studentName = mystudent.getName();
        String studentAddr = mystudent.getAddress();
        int studentAge = mystudent.getAge();
        double studentGpa = mystudent.getGpa();
        
        System.out.println("Student: "+studentName+" lives at "+studentAddr+" is "+studentAge+" year(s) old and has a gpa of "+studentGpa);
    }
    
    // Intialize the student class
    public static class Student{
        
        // Data fields
        String name;
        String address;
        int age;
        double gpa;
        
        // Create the constructors
        Student(){
            name="John Doe";
            address="123 ABC St";
            age=6;
            gpa=0.00;
        }
        Student(String name,String address,int age,double gpa){
            this.name=name;
            this.address=address;
            this.age=age;
            this.gpa=gpa;
        }
        
        // Get Methods
        String getName(){
            return name;
        }
        String getAddress(){
            return address;
        }
        int getAge(){
            return age;
        }
        double getGpa(){
            return gpa;
        }
        
        // Set Methods
        void setName(String name){
            this.name=name;
        }
        void setAddress(String address){
            this.address=address;
        }
        void setAge(int age){
            this.age=age;
        }
        void setGpa(double gpa){
            this.gpa=gpa;
        }
    }
}
