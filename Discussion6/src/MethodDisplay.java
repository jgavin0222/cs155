/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class MethodDisplay {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Operation (Add,Sub,Mul,Div): ");
        String type = input.next();
        System.out.print("Two integers: ");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        
        // Heart of the program
        switch(type.toLowerCase()){
            case "add":{
                output("addition",(num1+num2));
                break;
            }
            case "sub":{
                output("subtraction",(num1-num2));
                break;
            }
            case "mul":{
                output("multiplication",(num1*num2));
                break;
            }
            case "div":{
                output("division",(int)(num1/num2));
                break;
            }
            default:{
                System.out.println("Invalid operation");
            }
        }
    }
    
    // Ouput method
    private static void output(String type,int value){
        System.out.println("The "+type+" equals "+value);
    }
}
