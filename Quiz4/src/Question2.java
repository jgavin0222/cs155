/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get the user input
        System.out.print("Enter a letter: ");
        String userString = input.next();
        char userChar = userString.charAt(0);
        char upperChar = Character.toUpperCase(userChar);
        
        switch(upperChar){
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U':{
                System.out.println(userChar+" is a vowel");
                break;
            }
            case 'B':
            case 'C':
            case 'D':
            case 'F':
            case 'G':
            case 'H':
            case 'J':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'P':
            case 'Q':
            case 'R':
            case 'S':
            case 'T':
            case 'V':
            case 'W':
            case 'X':
            case 'Y':
            case 'Z':{
                System.out.println(userChar+" is a consonant");
                break;
            }
            default:{
                System.out.println(userChar+" is an invalid input");
            }
        }
        
    }
}
