/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter a telephone number: ");
        String userPhone = input.next();
        
        // Check for valid phone number
        if(userPhone.matches("(\\d{3})(-)(\\d{3})(-)(\\d{4})")){
            System.out.println(userPhone+" is a valid telephone number");
        }else{
            System.out.println(userPhone+" is an invalid telephone number");
        }
        
    }
}
