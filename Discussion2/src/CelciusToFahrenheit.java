/**
 *
 * @author JackGavin
 */

import java.util.Scanner;

// Initialize the class
public class CelciusToFahrenheit {
    
    // Intialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter a temperature in celcius: ");
        double celcius = input.nextDouble();
        
        // Calculation
        double fahrenheit = (celcius * 9 / 5) + 32;
        
        // Output
        System.out.println(celcius+" degrees celcius is "+fahrenheit+" degrees fahrenheit");
        
    }
}
