/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Calculate
        double operation = (9.5 + 4.5 - 2.5 * 3) / (45.5 * 3.5 * 2.5);
        
        // Output the calculation
        System.out.println(operation);
        
    }
    
}
