/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question3 {
    
    // Intialize the main method
    public static void main(String[] args){
        
        // Set constants
        double width = 7.7;
        double height = 12.4;
        
        // Run calculation
        double perimeter = (width*2) + (height*2);
        double area = width * height;
        
        // Output
        System.out.println("Permiter is: "+ perimeter);
        System.out.println("Area is: "+area);
        
    }
    
}
