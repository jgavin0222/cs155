/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question4 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Set constants
        double distanceKm = 30;
        double hours = 1;
        double minutes = 10;
        double seconds = 35;
        
        // Calculate for time in hours as a decimal
        double time = hours + (minutes/60) + (seconds/3600);
        
        // Calculate
        double miles = 30/1.6; // We have km so divide by 1.6 to get miles
        double speed = miles/time;
        
        // Output
        System.out.println("Speed in miles per hour is: "+speed);
        
    }
    
}
