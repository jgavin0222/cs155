/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Requirement2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Define the array
        int[] array = {8, 3, 5, 1, 9, 2, 6, 4, 7};
        
        // Run the first for loop for switching
        for(int i=0;i<array.length-1;i++){
            
            // Get the value of the index in the array
            int number = array[i];
            // Set initial switch value to itself
            int switchPosition=i;
            // Iterate for the rest of the array looking for lower numbers
            for(int j=i;j<array.length;j++){
                
                // If a lower number is found update number, and position
                if(number>array[j]){
                    number = array[j];
                    switchPosition=j;
                }
            }
            
            // Swap the two values
            int swapA = array[switchPosition];
            int swapB = array[i];
            array[i]=swapA;
            array[switchPosition]=swapB;
        }
        
        // Print the array
        for(int i=0;i<array.length;i++){
            System.out.print(array[i]+" ");
        }
    }
}
