/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter list 1 size and contents: ");
        int lengthOne = input.nextInt();
        int[] arrayOne = new int[lengthOne];
        for(int i=0;i<lengthOne;i++){
            arrayOne[i]=input.nextInt();
        }
        
        // Get user input
        System.out.print("Enter list 2 size and contents: ");
        int lengthTwo = input.nextInt();
        
        // Check the length of both arrays
        if(lengthTwo==lengthOne){
            
            // Iterate through the second array to compare against the first
            for(int i=0;i<lengthTwo;i++){
                
                // Set the value of the incoming number
                int number = input.nextInt();
                
                // Compare the two values
                if(number!=arrayOne[i]){
                    System.out.println("Two lists are not identical");
                    System.exit(0);
                }else if(i==(lengthTwo-1)){
                    // If the value is the last value, and matches, the list must be identical 
                    System.out.println("Two lists are identical");
                }
            }
        }else{
            System.out.println("Two lists are not identical");
        }
    }
}
