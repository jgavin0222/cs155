/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter 10 numbers: ");
        double[] numbers = new double[10];
        for(int i=0;i<10;i++){
            numbers[i]=input.nextDouble();
        }
        
        // Output the value of the methods
        System.out.println("The minimum number is: "+min(numbers));
        System.out.println("The maximum number is: "+max(numbers));
        System.out.println("The average is: "+average(numbers));
    }
    
    
    // Minimum method
    public static double min (double[] array){
        double min = 0;
        for(int i=0;i<array.length;i++){
            if((array[i]<min)||(i==0)){
                min=array[i];
            }
        }
        return min;
    }
    
    // Maximum method
    public static double max (double[] array){
        double max = 0;
        for(int i=0;i<array.length;i++){
            if((array[i]>max)||(i==0)){
                max=array[i];
            }
        }
        return max;
    }
    
    // Average method
    public static double average(double[] array){
        double zero = 0;
        for(int i=0;i<array.length;i++){
            zero+=array[i];
        }
        return zero/10;
    }
}
