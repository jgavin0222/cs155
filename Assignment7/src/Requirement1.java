/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Requirement1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Define the array
        char[] startArray = {'b', 'e', 'x', 'q', 'u', 'a', 'p', 'z'};
        
        // Call the array to get position index
        int position = getPosition(startArray,'a');
        
        // Analyize the result
        if(position==-1){
            System.out.println("Character is not present within the array");
        }else{
            System.out.println("Character is located at "+position);
        }
    }
    
    // Method to return the postition of the character
    private static int getPosition(char[] array,char type){
        
        // If nothing matches return -1
        int position=-1;
        
        // Heart of the program
        for(int i=0;i<array.length;i++){
            
            // Check if characters match, if they do update position
            if(Character.toLowerCase(array[i])==Character.toLowerCase(type)){
                position=i;
            }
        }
        return position;
    }
}
