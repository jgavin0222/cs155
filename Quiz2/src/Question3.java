/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question3 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter a value for feet: ");
        double meters = input.nextDouble();
        double feet = meters/0.305;
        
        // Output
        System.out.println(meters+" meters is "+feet+" feet");
        
    }
}
