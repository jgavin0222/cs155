/**
 *
 * @author JackGavin
 */

// Import the scanner and decimals
import java.util.Scanner;
import java.text.DecimalFormat;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the Scanners and Decimal Formats constructor
        Scanner input = new Scanner(System.in);
        DecimalFormat decimals = new DecimalFormat(".###");
        
        // Get user input
        System.out.print("Enter speed and acceleration : ");
        double speed = input.nextDouble();
        double acceleration = input.nextDouble();
        
        // Calculation
        double squareSpeed = Math.pow(speed,2);
        double doubleAccel = 2*acceleration;
        double length = squareSpeed/doubleAccel;
        
        // Out the result
        System.out.println("The minimum runway length for this airplane is "+decimals.format(length));
        
    }
}
