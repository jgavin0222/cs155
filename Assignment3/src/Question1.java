/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor method
        Scanner input = new Scanner(System.in);
        
        // Get user input for package 1
        System.out.print("Enter weight and price for package 1: ");
        double package1Weight = input.nextDouble();
        double package1Price = input.nextDouble();
        
        // Get user input for package 2
        System.out.print("Enter weight and price for package 2: ");
        double package2Weight = input.nextDouble();
        double package2Price = input.nextDouble();
        
        // Calculate price to pound per package
        double package1Ratio = package1Weight / package1Price;
        double package2Ratio = package2Weight / package2Price;
        
        // Decide which is better
        if(package1Ratio > package2Ratio){
            System.out.println("Package 1 has a better price");
        }else{
            System.out.println("Package 2 has a better price");
        }
        
    }
    
}
