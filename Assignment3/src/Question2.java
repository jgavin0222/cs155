/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Intialize the scanner
        Scanner input = new Scanner(System.in);
        
        // Get user input to switch currency type
        System.out.print("Enter 1 to convert from U.S. dollars to Chinese yuan, 2 to convert from U.S. to French Euro, 3 to  convert from U.S. dollars to England pound, 4 to convert from U.S. dollar to Japanese Yen, and 5 to convert from U.S. dollars to Brazil Real: ");
        int currencyOption = input.nextInt();
        
        // Get exchange rate and dollar amount
        System.out.print("Enter exhcange rate: ");
        double exchangeRate = input.nextDouble();
        System.out.print("Enter dollar amount: ");
        double dollars = input.nextDouble();
        
        // Calculate the dollar exchange
        double exchangeAmount = exchangeRate * dollars; 
        
        // Switch user input
        switch(currencyOption){
            case 1:{
                output(dollars,exchangeAmount,"yuan");
                break;
            }
            case 2:{
                output(dollars,exchangeAmount,"Euro");
                break;
            }
            case 3:{
                output(dollars,exchangeAmount,"pound");
                break;
            }
            case 4:{
                output(dollars,exchangeAmount,"Yen");
                break;
            }
            case 5:{
                output(dollars,exchangeAmount,"Real");
                break;
            }
            default:{
                System.out.println("Invalid operation, please try again");
            }
        }
         
    }
    
    // output method to clean up switch statement
    public static void output(double dollars,double exchangeAmount,String currency){
            System.out.println("$"+dollars+" is "+exchangeAmount+" "+currency);
    }
    
}
