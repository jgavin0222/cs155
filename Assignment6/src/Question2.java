/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter some millies: ");
        long millies = input.nextLong();
        System.out.println(millies+" millies converts to: "+convertMillies(millies));
    }
    
    // Helper function
    public static String convertMillies(long millies){
        long convertedMillies = millies/1000;
        long seconds = convertedMillies%60;
        convertedMillies = convertedMillies/60;
        long minutes = convertedMillies%60;
        long hours = convertedMillies/60;
        return hours+":"+minutes+":"+seconds;
    }
}
