/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input 
        System.out.print("Enter the side: ");
        double side = input.nextDouble();
        System.out.println("The area of the pentagon is: "+area(side));
    }
    
    public static double area(double side){
        double squareSide = Math.pow(side,2);
        double numerator = 5 * squareSide;
        double radians = Math.PI/5;
        double angle = Math.tan(radians);
        double denominator = 4 * angle;
        double result = numerator/denominator;
        return result;
    }
}
