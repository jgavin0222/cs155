/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter a string: ");
        String userInput = input.nextLine();
        
        // Get string length
        int strLength = userInput.length();
        
        // Define variables
        int vowels = 0;
        int consonants = 0;
        
        // For loop
        for(int i=0;i<strLength;i++){
            char character = userInput.charAt(i);
            char lowerCaseChar = Character.toLowerCase(character);
            switch(lowerCaseChar){
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':{
                    vowels++;
                    break;
                }
                case ' ':{
                    break;
                }
                default:{
                    consonants++;
                }
            }
        }
        System.out.println("The number of vowels is "+vowels);
        System.out.println("The number of consonants is "+consonants);
    }
}
