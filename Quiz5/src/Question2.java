/**
 *
 * @author JackGavin
 */

// Initalize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Define how many times to run
        int runNumber = 5;
        
        // Heart of the program
        for(int i=runNumber;i>0;i--){
            // Define output string
            String output = "";
            
            // Run again equivalent to i
            for(int j=0;j<i;j++){
                output=output+""+i;
            }
            System.out.println(output);
        }
    }
}
