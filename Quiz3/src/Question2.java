/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constuctor
        Scanner input = new Scanner(System.in);
        
        // Prompt the user
        System.out.print("Enter 1 to add two numbers, 2 to subtract second number from first, 3 to  multiply the two numbers, 4 to divide the first number by the second (assuming that the second is not zero):");
        int operation = input.nextInt();
        
        // Get the numbers
        System.out.print("Enter two numbers: ");
        int firstNum = input.nextInt();
        int secondNum = input.nextInt();
        
        switch(operation){
            case 1:{
                output("summing",(firstNum+secondNum));
                break;
            }
            case 2:{
                output("subtracting",(firstNum-secondNum));
                break;
            }
            case 3:{
                output("multiplying",(firstNum*secondNum));
                break;
            }
            case 4:{
                output("dividing",((int)firstNum/secondNum));
                break;
            }
            default:{
                System.out.println("Invalid Operation");
            }
        }
        
    }
    
    public static void output(String type, int result){
        System.out.println("The result of "+type+" the two numbers is: "+result);
    }
    
}
