/**
 *
 * @author JackGavin
 */

// import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get shop 1 weight and price
        System.out.print("Enter weight and price from shop 1: ");
        double shop1Weight = input.nextDouble();
        double shop1Price = input.nextDouble();
        double calculate1 = shop1Weight / shop1Price;
        
        // Get shop 2 weight and price
        System.out.print("Enter weight and price from shop 2: ");
        double shop2Weight = input.nextDouble();
        double shop2Price = input.nextDouble();
        double calculate2 = shop2Weight / shop2Price;
        
        // Get shop 3 weight and price
        System.out.print("Enter weight and price from shop 3: ");
        double shop3Weight = input.nextDouble();
        double shop3Price = input.nextDouble();
        double calculate3 = shop3Weight / shop3Price;
        
        // Calculate
        if((calculate1 >= calculate2) && (calculate1 >= calculate3)){
            System.out.println("Package 1 has a better price.");
        }else if((calculate2 >= calculate1) && (calculate2 >= calculate3)){
            System.out.println("Package 2 has a better price.");
        }else if((calculate3 >= calculate1) && (calculate3 >= calculate2)){
            System.out.println("Package 3 has a better price.");
        }else{
            System.out.println("Could not determine the best deal");
        }
        
    }
    
}
