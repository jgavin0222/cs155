/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Set variables
        int iterations = 10;
        
        // Output to user
        System.out.println("The result of summing 1..."+iterations+" with sum i/1 is "+series(iterations));
    }
    
    // Helper method
    public static double series(int a){
        double value = 1 / (double)a;
        if(a==1){
            return 1;
        }else{
            return value + series(a-1);
        }
    }
}
