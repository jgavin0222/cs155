/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Task1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Set the value of i
        int i = 0;
        
        // While loop
        while(i<=10){
            
            // Output the value
            System.out.print(i+" ");
            
            // Increment the value of i
            i++;
        }
    }
}
