/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Task3 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners main method
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("How many rows: ");
        int rows = input.nextInt();
        
        // Set constants
        int spaces = rows-1;
        int stars = 1;
        
        // Heart of the program
        for(int i=0;i<rows;i++){
            String output="";
            for(int j=0;j<spaces;j++){
                output=output+" ";
            }
            for(int j=0;j<stars;j++){
                output=output+"*";
            }
            System.out.println(output);
            spaces--;
            stars=stars+2;
        }
    }
}
