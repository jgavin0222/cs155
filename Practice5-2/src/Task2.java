/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Task2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Define userInput
        int userInput = 0;
        
        // Run the while statement forever
        do {
            
            // Prompt user for input
            System.out.println("choose 1 to print Hi");
            System.out.println("choose 2 to print Hello");
            System.out.println("choose 3 to print Hi and Hello");
            System.out.println("choose 4 to exit.");
            System.out.print("Enter your selection: ");
            
            // Set user input value
            userInput = input.nextInt();
            
            // Main switch statement
            switch(userInput){
                case 1:{
                    System.out.println("Hi");
                    break;
                }
                case 2:{
                    System.out.println("Hello");
                    break;
                }
                case 3:{
                    System.out.println("Hi and Hello");
                    break;
                }
                case 4:{
                    System.out.println("System will exit");
                    break;
                }
                default:{
                    System.out.println("Please enter a valid option");
                }
            }
        } while (userInput != 4);
    }
}
