/**
 *
 * @author jackgavin
 */

// Import datetime
import java.time.LocalDateTime;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Create a new balance
        Balance b = new Balance(11223,10.5,50.90,20,10,8);
        
        // Print result
        System.out.println("Balance total is: "+b.getTotalPrice());
        System.out.println("Item 1:  Price: "+b.getItem1Price()+ "  Quantity: "+b.getItem1Quantity());
        System.out.println("Item 2:  Price: "+b.getItem2Price()+ "  Quantity: "+b.getItem2Quantity());
        System.out.println("Balance created on: "+b.getDateCreated());
        System.out.println("Tax: "+b.getTaxPercentage());
    }

    // Balance class
    public static class Balance{
        
        // Create datafields
        private int id;
        private final LocalDateTime DATE_CREATED;
        private double totalPrice;
        private double item1Price;
        private double item2Price;
        private int item1Quantity;
        private int item2Quantity;
        private double taxPercentage;
        
        // Create constructors
        Balance(){
            this.DATE_CREATED = LocalDateTime.now();
            id = 0;
            totalPrice = 0;
            item1Price = 0;
            item2Price = 0;
            item1Quantity = 0;
            item2Quantity = 0;
        }
        Balance(int id,double item1Price,double item2Price,int item1Quantity,int item2Quantity,double taxPercentage){
            this.DATE_CREATED = LocalDateTime.now();
            this.id = id;
            this.item1Price = item1Price;
            this.item2Price = item2Price;
            this.item1Quantity = item1Quantity;
            this.item2Quantity = item2Quantity;
            this.taxPercentage = taxPercentage/100;
        }
        
        // Class methods
        void setId(int id){
            this.id = id;
        }
        void setItem1Price(double item1Price){
            this.item1Price = item1Price;
        }
        void setItem2Price(double item2Price){
            this.item2Price = item2Price;
        }
        void setItem1Quantity(int item1Quantity){
            this.item1Quantity = item1Quantity;
        }
        void setItem2Quantity(int item2Quantity){
            this.item2Quantity = item2Quantity;
        }
        void setTaxPercentage(double taxPercentage){
            this.taxPercentage = taxPercentage/100;
        }
        int getId(){
            return id;
        }
        double getItem1Price(){
            return item1Price;
        }
        double getItem2Price(){
            return item2Price;
        }
        int getItem1Quantity(){
            return item1Quantity;
        }
        int getItem2Quantity(){
            return item2Quantity;
        }
        String getDateCreated(){
            return DATE_CREATED.toString();
        }
        String getTaxPercentage(){
            return taxPercentage*100+"% ("+taxPercentage+")";
        }
        double getTotalPrice(){
            double item1Cost = (item1Price * item1Quantity);
            double item2Cost = (item2Price * item2Quantity);
            double subtotal = item1Cost + item2Cost;
            double taxCost = subtotal * taxPercentage;
            double total = subtotal + taxCost;
            return total;
        }
    }
}
