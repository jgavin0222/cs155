/**
 *
 * @author jackgavin
 */

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Create a new squares
        Square s1 = new Square(4);
        Square s2 = new Square(6.5);
        
        // Display outputs
        System.out.println("Square 1: length: "+s1.length+" perimeter: "+s1.getPerimeter()+" area: "+s1.getArea());
        System.out.println("Square 2: length: "+s2.length+" perimeter: "+s2.getPerimeter()+" area: "+s2.getArea());
    }
    
    // Square class
    public static class Square{
        
        // Define datafields
        double length;
        
        // Create constructors
        Square(){
            length = 1;
        }
        Square(double length){
            this.length = length;
        }
        
        // Class methods
        double getArea(){
            return Math.pow(length,2);
        }
        double getPerimeter(){
            return 4*length;
        }
    }
}
