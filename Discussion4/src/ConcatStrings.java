/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class ConcatStrings {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Cal the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Ask the user for two strings
        System.out.print("Enter a string: ");
        String stringA = input.next();
        System.out.print("Enter a string: ");
        String stringB = input.next();
        
        // Concat and output
        String combine = stringA.concat(stringB);
        System.out.println("Concatenated result: "+combine);
        
    }
}
