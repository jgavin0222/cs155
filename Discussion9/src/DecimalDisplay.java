/**
 *
 * @author jfgavin1s
 */
// Import the scanner and decimal format
import java.util.Scanner;
import java.text.DecimalFormat;

// Initialize the class
public class DecimalDisplay {

    // Intialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get a decimal from the user
        System.out.print("Please enter a decimal value: ");
        double userInput = input.nextDouble();

        // Call decimal formats constructor with pattern
        String format = "#.##";
        DecimalFormat dm = new DecimalFormat(format);

        // Use decimal format to format user input
        System.out.println("Formatting "+userInput+" to 2 decimal places.");
        String decimal = dm.format(userInput);

        // Output the final value
        System.out.println("The formatted value is: "+decimal);
    }
}