/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class ConditionalCalculator {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get operation from user
        System.out.print("Enter an operation (add/subtract/multiply/divide): ");
        String operation = input.next();
        System.out.print("Enter two integers: ");
        int firstNum = input.nextInt();
        int secondNum = input.nextInt();
        
        // Switch operation
        switch(operation){
            case "add":{
                System.out.println(firstNum+" + "+secondNum+" = "+(firstNum+secondNum));
                break;
            }
            case "subtract":{
                System.out.println(firstNum+" - "+secondNum+" = "+(firstNum-secondNum));
                break;
            }
            case "multiply":{
                System.out.println(firstNum+" * "+secondNum+" = "+(firstNum*secondNum));
                break;
            }
            case "divide":{
                System.out.println(firstNum+" / "+secondNum+" = "+(firstNum/secondNum));
                break;
            }
        }
    }
}
