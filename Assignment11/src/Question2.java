
/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question2 {

    // Initialize the main method
    public static void main(String[] args) {

        // Define variables
        int iterations = 10;

        // Output the result to the user
        System.out.println("Number series i/(2i+1)");
        System.out.println("Let i = " + iterations);
        System.out.println("The result is: " + numberSeries(iterations));
    }

    public static double numberSeries(int a) {
        double bottom = (2 * a + 1);
        double fraction = a / bottom;
        if (a == 0) {
            return fraction;
        } else {
            return fraction + numberSeries(a - 1);
        }
    }
}
