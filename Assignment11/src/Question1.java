
/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {

    // Initialize the main method
    public static void main(String[] args) {

        // Call the scanners constructor
        Scanner input = new Scanner(System.in);

        // Get user input
        System.out.print("Please enter two integers: ");
        int a = input.nextInt();
        int b = input.nextInt();

        // Get gcd and output
        double result = gcd(a, b);
        System.out.println("The gcd of " + a + " and " + b + " is " + result);
    }

    public static double gcd(double a, double b) {
        if (a % b == 0) {
            return a;
        } else {
            return gcd(a, a % b);
        }
    }
}
