/**
 *
 * @author jackgavin
 */

// Intialize the class
public class Question1 {
    
    // Intialize the main method
    public static void main(String[] args){
        
        // Create rectangles
        Rectangle rec1 = new Rectangle(40,4);
        Rectangle rec2 = new Rectangle(35.9,3.5);
        
        // Output result
        System.out.println("The first rectangle has a height of "+rec1.height+" and a width of "+rec1.width+" with an area of "+rec1.getArea()+" and a permiter of "+rec1.getPerimiter());
        System.out.println("The second rectangle has a height of "+rec2.height+" and a width of "+rec2.width+" with an area of "+rec2.getArea()+" and a permiter of "+rec2.getPerimiter());
    }
    
    // Create the rectangle class
    public static class Rectangle {
        
        // Initialize default data fields
        double height;
        double width;
        
        // Create the constructors
        Rectangle(){
            height = 1;
            width = 1;
        }
        Rectangle(double a,double b){
            height = a;
            width = b;
        }
        
        // Create methods
        double getArea(){
            return height*width;
        }
        double getPerimiter(){
            return (2*height)+(2*width);
        }
    }
}

