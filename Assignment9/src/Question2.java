/**
 *
 * @author jackgavin
 */

// Import datetime
import java.time.LocalDateTime;

// Intialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Create the account and output the balance
        Account a1 = new Account(1122,20000);
        System.out.println("Current balance: $"+a1.getBalance());
        
        // Set the interest rate
        Account.setAnnualInterestRate(4.5);
        
        // Withdraw
        a1.withdraw(2500);
        System.out.println("Balance after withdrawing $2500: $"+a1.getBalance());
        
        // Deposit
        a1.deposit(3000);
        System.out.println("Balance after depositing $3000: $"+a1.getBalance());
        
        // Print monthly interest and creation date
        System.out.println("Monthly interest: $"+a1.getMounthlyInterest());
        System.out.println("Creation date: "+a1.getDateCreated());
        
    }
    
    public static class Account{
        
        // Initialize data fields
        private int id;
        private double balance;
        private LocalDateTime dateCreated;
        private static double annualInterestRate = 0;
        
        // Create constructors
        Account(){
            id=0;
            balance=0;
            dateCreated = LocalDateTime.now();
        }
        Account(int idI,double balanceI){
            id = idI;
            balance = balanceI;
            dateCreated = LocalDateTime.now();
        }
        
        int getId(){
            return id;
        }
        
        void setId(int idI){
            id = idI;
        }
        
        double getBalance(){
            return balance;
        }
        
        void setBalance(double balanceI){
            balance = balanceI;
        }
        
        double getAnnualInterestRate(){
            return annualInterestRate;
        }
        
        static void setAnnualInterestRate(double annualInterestRateI){
            annualInterestRate = annualInterestRateI/100;
        }
        
        LocalDateTime getDateCreated(){
            return dateCreated;
        }
        
        double getMonthlyInterestRate(){
            return annualInterestRate/12;
        }
        
        double getMounthlyInterest(){
            return getMonthlyInterestRate() * balance;
        }
        
        void withdraw(double amount){
            balance = balance-amount;
        }
        
        void deposit(double amount){
            balance = balance+amount;
        }
    }
}
