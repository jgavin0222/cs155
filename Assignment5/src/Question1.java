/**
 *
 * @author JackGavin
 */

// import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Set global variables
        int digits = 0;
        int lowerCaseLetters = 0;
        int upperCaseLetters = 0;
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Prompt the user for input
        System.out.print("Enter a string: ");
        String userString = input.nextLine();
        int userStringLength = userString.length();
        
        // Get number of spaces
        int spaces = userString.split(" ").length-1;
        
        // Main for loop
        for(int i=0;i<userStringLength;i++){
            
            // Get character
            char character = userString.charAt(i);
            
            // Check if character is uppercase
            if(Character.isUpperCase(character)){
                upperCaseLetters++;
            }else if(Character.isLowerCase(character)){
                lowerCaseLetters++;
            }else if(Character.isDigit(character)){
                digits++;
            }
        }
        
        // Output the results
        System.out.println("The number of uppercase letters is "+upperCaseLetters);
        System.out.println("The number of lowercase letters is "+lowerCaseLetters);
        System.out.println("The number of spaces is "+spaces);
        System.out.println("The number of digits is "+digits);
    }
}
