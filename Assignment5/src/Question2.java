/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the first pattern
        System.out.println("Pattern 1:");
        pattern1(); 
        
        // Call the second pattern
        System.out.println("Pattern 2:");
        pattern2();
    }
    
    private static void pattern1(){
        
        // First for loop
        for(int i=1;i<=5;i++){
            // Second for loop
            for(int j=0;j<i;j++){
                System.out.print(i);
            }
            System.out.println();
        }
    }
    
    private static void pattern2(){
        
        // Define the target
        int target = 5;
        for(int i=1;i<=target;i++){
            String out = "";
            int diff = target-i;
            for(int j=0;j<diff;j++){
                out=out+" ";
            }
            for(int j=0;j<i;j++){
                out=out+i;
            }
            System.out.println(out);
        }
    }
}