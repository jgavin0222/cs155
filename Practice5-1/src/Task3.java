/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Task3 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Define the value of i
        int i = 10;
        while(i>=0){
            
            // Print the value of i
            System.out.println(i);
            
            // Decrement the value of i
            i--;
        }        
    }    
}
