/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Task1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Define the value of i
        int i = 0;
        
        // Heart of the program
        while(i<=20){
            
            // Output the value
            System.out.print((i*5)+" ");
            
            // Increment i
            i++;
        }
    }
}
