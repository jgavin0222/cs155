/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Task2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Define the value of i and max
        int i=0;
        int max = 10;
        
        // Main while loop
        while(i<=max){
            
            // Heart of the program
            if(i==max){
                System.out.print((i*100));
            }else{
                System.out.print((i*100)+", ");
            }
            
            // Increment i
            i++;
            
        }        
    }   
}
