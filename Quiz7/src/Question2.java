/**
 *
 * @author JackGavin
 */

// import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        double[] array = new double[10];
        System.out.print("Enter 10 numbers: ");
        for(int i=0;i<10;i++){
            array[i]=input.nextDouble();
        }
        
        // Output the result
        System.out.println("The minimum number is: "+min(array));
        int iteration = minLocation(array);
        if(iteration==-1){
            System.out.println("There is no minimum number");
        }else{
            System.out.println("location of minimum number is: "+iteration);
        }
    }
    public static double min (double[] array){
        double min = 0;
        for(int i=0;i<array.length;i++){
            if((array[i]<min)||(i==0)){
                min=array[i];
            }
        }
        return min;
    }
    public static int minLocation(double [] array){
        int iteration=-1;
        double min = 0;
        for(int i=0;i<array.length;i++){
            if((array[i]<min)||(i==0)){
                min=array[i];
                iteration=i;
            }
        }
        return iteration;
    }
}
