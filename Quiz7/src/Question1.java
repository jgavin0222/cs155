/**
 *
 * @author JackGavin
 */

// import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter list 1 size and contents: ");
        int length = input.nextInt();
        int[] arrayOne = new int[length];
        for(int i=0;i<length;i++){
            arrayOne[i]=input.nextInt();
        }
        
        System.out.print("Enter list 2 size and contents: ");
        int lengthTwo = input.nextInt();
        int[] arrayTwo = new int[lengthTwo];
        for(int i=0;i<length;i++){
            arrayTwo[i]=input.nextInt();
        }
        
        // Call the method
        boolean result = similar(arrayOne,arrayTwo);
        
        // Output the result to the user
        if(result){
            System.out.println("Two lists are similar");
        }else{
            System.out.println("Two lists are not similar");
        }
    }
    
    // Method to check if the two arrays are similar
    public static boolean similar (int[] list1, int[] list2){
        
        // Initialize an array of integers of previously used list2 values
        int[] prohibitedList2 = {};
        
        // Set an overall return boolean
        boolean falsify = false;
        
        // Loop through the first list of integers
        for(int i=0;i<list1.length;i++){
            
            // Set a local boolean to check if value is present
            boolean found=false;
            
            // Initialize an iteration for list2 previously used values
            int jIter = -1;
            
            // Loop through the second list
            for(int j=0;j<list2.length;j++){
                
                // Check if the two values being checked match
                if(list1[i]==list2[j]){
                    
                    // Initialize a boolean to falisfy if the value has been used
                    boolean prohibited = false;
                    
                    // Loop through the previously used values
                    for(int k=0;k<prohibitedList2.length;k++){
                        
                        // Check if the list2 successfully value has been used
                        if(prohibitedList2[k]==j){
                            
                            // If the value has been used, flip the prohibited boolean
                            prohibited=true;
                        }
                    }
                    
                    // Check if the value was prohibited
                    if(!prohibited){
                        
                        // If the value was not prohibited, flip the found boolean to true
                        found=true;
                        
                        // Set the j iteration value to prohibit this list2 value in the future
                        jIter=j;
                    }
                }
            }
            
            // Check if the value was found
            if(!found){
                
                // If the value was not found, set the global falisfy to return value to false
                falsify=true;
            }else{
                
                // If the value was found, add the j iteration value to the prohibited list2 values array
                prohibitedList2 = push(prohibitedList2,jIter);
            }
        }
        
        // After both loops have completed, check if the falsify value is flipped
        if(!falsify){
            return true;
        }else{
            return false;
        }
    }
    
    // Push value into array, and return the a new array with the value
    private static int[] push(int[] a,int b){
        
        // Initialize a new array, one longer than the original
        int[] c=new int[a.length+1];
        
        // Loop through the old array
        for(int i=0;i<a.length;i++){
            // Place values into the new array
            c[i]=a[i];
        }
        // Add the value to be "pushed"
        c[a.length]=b;
        // Return the new array
        return c;
    }
}
