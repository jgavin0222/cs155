/**
 *
 * @author JackGavin
 */
import java.util.Scanner;
public class Average {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of rows and columns of the array: ");
        int rows = input.nextInt();
        int columns = input.nextInt();
        
        // Decare the array
        double[][] array = new double[rows][columns];
        
        // Map input values to array
        for(int i=0;i<rows;i++){
            System.out.print("Enter values for row #"+i+": ");
            for(int j=0;j<columns;j++){
                array[i][j]=input.nextDouble();
            }
        }
        
        // Define variables
        int[] valuesInRow = new int[rows];
        double[] rowTotal = new double[rows];
        int[] valuesInColumn = new int[columns];
        double[] columnTotal = new double[columns];
        
        // Get average of rows
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                valuesInRow[i]++;
                rowTotal[i]+=array[i][j];
            }
        }
        
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                valuesInColumn[i]++;
                columnTotal[i]+=array[j][i];
            }
        }
        
        // Output the entire array
        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                System.out.print(array[i][j]+" ");
            }
            System.out.println();
        }
        
        // Output average
        for(int i=0;i<rows;i++){
            System.out.println("Row "+i+" average: "+(rowTotal[i]/valuesInRow[i]));
        }
        for(int i=0;i<columns;i++){
            System.out.println("Column "+i+" average: "+(columnTotal[i]/valuesInColumn[i]));
        }
    }
}
