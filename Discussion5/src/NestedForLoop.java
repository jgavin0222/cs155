/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class NestedForLoop {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get the user input
        System.out.print("How many times would like to iterate: ");
        int runNumber = input.nextInt();
        
        
        // Heart of the program
        for(int i=1;i<=runNumber;i++){
            String outputString = "";
            for(int j=0;j<i;j++){
                outputString += ""+i;
            }
            System.out.println(outputString);
        }
    }
}
