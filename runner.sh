#!/bin/bash
fail=0
total=0
maindir=$(pwd)
echo $maindir
for javaFile in `find ./ -name *.java`
do
	dirnamex=$(dirname ${javaFile})
	cd $maindir${dirnamex:1}
	javac $(basename ${javaFile})
	if [ $? -eq 0 ]; then
		echo -ne .
	else
		echo -ne X
		files="";for file in ./*.java; do files="$files $file"; done;echo $files
		javac -cp . $files
		if [ $? -eq 0 ]; then
			echo -ne P
		else
			fail=$(($fail + 1))
		fi
	fi
	total=$(($total+1))
done
cd $maindir
find . -name '*.class' -exec rm -f {} \;
echo
if [ $fail -eq 0 ]; then
	echo Success
	echo Coverage: 100%
	exit 0;
else
	echo Failure
	echo $total tests ran
	echo $fail tests failed
	minus=$(expr $total - $fail)
	echo $minus tests passed
	exit 1;
fi