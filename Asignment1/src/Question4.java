/**
 *
 * @author JackGavin
 */
public class Question4 {
    public static void main(String[] args){
        // Define Constants
        double distance = 70;
        double hours = 1;
        double minutes = 40;
        double seconds = 35;
        
        // Convert constants
        double kmDistance = distance * 1.6;
        double time = hours + (minutes/60) + (seconds/3600);
        
        // Output conversion
        System.out.println(kmDistance/time);
    }
}
