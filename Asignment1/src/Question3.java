/**
 *
 * @author JackGavin
 */
public class Question3 {
    public static void main(String[] args){
        double width = 4.5;
        double height = 7.9;
        
        // Calculate Permiter
        double perim = (2*width) + (2*height);
        
        // Calculate Area
        double area = width * height;
        
        // Output
        System.out.println("Permiter is: "+perim);
        System.out.println("Area is: "+area);
    }
}
