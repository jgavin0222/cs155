/**
 *
 * @author JackGavin
 */

// Initiate the class
public class Question2 {
    
    // Initiate the main method
    public static void main(String[] args){
        
        // Calculate
        double Result = (9.5 * 4.5 - 2.5 * 3) / (45.5 - 3.5);
        
        // Output
        System.out.println("The result is: "+Result);
    }
}