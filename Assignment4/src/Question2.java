/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Initialize the scanner
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter a year: ");
        int year = input.nextInt();
        System.out.print("Enter a month: ");
        String userMonth = input.next();
        
        // Heart of the program
        switch(userMonth){
            case "Jan":
            case "Mar":
            case "May":
            case "July":
            case "Aug":
            case "Oct":
            case "Dec":{
                System.out.println(userMonth+" "+year+" has 31 days");
                break;
            }
            case "Apr":
            case "Jun":
            case "Sep":
            case "Nov":{
                System.out.println(userMonth+" "+year+" has 30 days");
                break;
            }
            case "Feb":{
                if(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)){
                    System.out.println(userMonth+" "+year+" has 29 days");
                }else{
                    System.out.println(userMonth+" "+year+" has 28 days");
                }
                break;
            }
            default:{
                System.out.println(userMonth+" is not a correct month name");
            }
        }
    }
}

