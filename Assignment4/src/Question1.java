/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        checkSsn();
    }
    
    // Heart of the program
    private static void checkSsn(){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get the users ssn
        System.out.print("Enter a SSN: ");
        String ssn = input.next();
        
        if(validate(ssn)){
            System.out.println(ssn+" is a valid ssn");
        }else{
            System.out.println(ssn+" is an invalid ssn");
            checkSsn();
        }
       
    }
    
    // Helper validator
    private static boolean validate(String ssn){
        return ssn.matches("(\\d{3})(-)(\\d{2})(-)(\\d{4})");
    }
}
