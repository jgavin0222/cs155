/**
 *
 * @author jackgavin
 */
// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Please enter the length and width of the array: ");
        int length = input.nextInt();
        int width = input.nextInt();
        
        // Define the array
        double[][] array = new double[length][width];
        
        // Loop through to get user to fill array
        System.out.println("Please enter the values for the array");
        for(int i=0;i<length;i++){
            for(int j=0;j<width;j++){
                array[i][j]=input.nextDouble();
            }
        }
        
        // Call the calculation method
        int[] positions = locateSmallest(array);
        if((positions[0]!=-1)&&(positions[1]!=-1)){
            System.out.println("The smallest value is located at "+'('+positions[0]+','+positions[1]+')');
        }
        
    }
    
    public static int[] locateSmallest(double [][] a){
        
        // Define variables at the start of the method
        double smallest = 0;
        int x = -1;
        int y = -1;
        
        // Loop through the array to find the smallest value
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a[i].length;j++){
                if((i==0 && j==0)||(a[i][j]<smallest)){
                    x=i;
                    y=j;
                    smallest = a[i][j];
                }
            }
        }
        
        // Prepare to return position of smallest value
        int[] returnData = new int[2];
        returnData[0] = x;
        returnData[1] = y;
        
        // Return position
        return returnData;
    }
}
