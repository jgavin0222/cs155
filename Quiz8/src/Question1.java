/**
 *
 * @author jackgavin
 */
// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Please enter the length and width of the arrays: ");
        int length = input.nextInt();
        int width = input.nextInt();
        double[][] firstArray = getInput(length,width);
        double[][] secondArray = getInput(length,width);
        double[][] thirdArray = getInput(length,width);
        
        // Run the calculation
        double[][] total = addMatrix(firstArray,secondArray,thirdArray);
        
        // Print the result
        for(int i=0;i<total.length;i++){
            for(int j=0;j<total[i].length;j++){
                System.out.print(total[i][j]+" ");
            }
            System.out.println();
        }
        
    }
    
    // User input method
    public static double[][] getInput(int length,int width){
        
        double[][] returnDouble = new double[length][width];
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter values for a multi dimensional array: ");
        
        // Get values
        for(int i=0;i<length;i++){
            for(int j=0;j<width;j++){
                returnDouble[i][j]=input.nextDouble();
            }
        }
        return returnDouble;
    }
    
    // Adder method
    public static double[][] addMatrix(double [][] a, double b[][] , double c[][]){
        double[][] result = new double[a.length][a[0].length];
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a[i].length;j++){
                result[i][j]=a[i][j]+b[i][j]+c[i][j];
            }
        }
        return result;
    }
}
