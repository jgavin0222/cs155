
import java.util.Scanner;
/**
 *
 * @author JackGavin
 */
public class Addition {
     // Initialize the class Addition
    public static void main(String[] args){
        // Initialize the main method
        
        // Initilize the scanner
        Scanner scanner = new Scanner(System.in);
        
        // Ask for first number
        System.out.print("Please enter a positive integer: ");
        
        // Save first number
        int firstNum = scanner.nextInt();
        
        // Ask for second number
        System.out.print("Please enter a second positive integer: ");
        
        // Save second number
        int secondNum = scanner.nextInt();
        
        // Add the two numbers
        int total = firstNum + secondNum;
        
        // Output the total
        System.out.println("The addition of "+firstNum+" and "+secondNum+" is "+total);
    }
}
