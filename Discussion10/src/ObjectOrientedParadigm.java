/**
 *
 * @author jackgavin
 */

// Initialize the class
public class ObjectOrientedParadigm {
    
    // Initialize the main method
    public static void main(String[] args){
        
        EqualateralTriangle t1 = new EqualateralTriangle(3);
        t1.setSide(5);
        System.out.println("Equalateral triangle with side length "+t1.getSide()+ " has a perimeter of "+t1.getPerimeter()+" and an area of "+t1.getArea());
        
    }
    
    public static class EqualateralTriangle{
         
        // Define datafields
        double sideLength;
        
        // Create constructor
        EqualateralTriangle(double sideLength){
            this.sideLength = sideLength;
        }
        
        // Class methods
        double getSide(){
            return sideLength;
        }
        double getPerimeter(){
            return sideLength*3;
        }
        double getArea(){
            return ((Math.sqrt(3)/4)*Math.pow(sideLength, 2));
        }
        void setSide(double sideLength){
            this.sideLength = sideLength;
        }
    }
}
