/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        MyPoint a = new MyPoint(0,0);
        MyPoint b = new MyPoint(10,30);
        
        double distance = MyPoint.distance(a, b);
        System.out.println("Points: ("+a.getX()+","+a.getY()+") and ("+b.getX()+","+b.getY()+") results in a distance of "+distance);
        
    }
    
    public static class MyPoint{
        
        // Datafields
        int x;
        int y;
        
        // Create constructors
        MyPoint(){
            x = 0;
            y = 0;
        }
        MyPoint(int x,int y){
            this.x = x;
            this.y = y;
        }
        
        // Class methods
        int getX(){
            return x;
        }
        int getY(){
            return y;
        }
        double distance(MyPoint a){
            return distanceCalc(x,y,a.getX(),a.getY());
        }
        double distance(int xB,int yB){
            return distanceCalc(x,y,xB,yB);
        }
        static double distance(MyPoint a,MyPoint b){
            return distanceCalc(a.getX(),a.getY(),b.getX(),b.getY());
        }
    }
    
    public static double distanceCalc(int xA,int yA,int xB,int yB){
        double xV = Math.pow((xA - xB),2);
        double yV = Math.pow((yA - yB),2);
        return Math.sqrt(xV+yV);
    }
}
