/**
 *
 * @author JackGavin
 */

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        int number = 21;
        int wrongNumber = 3;
        
        char[] characters = {'a','b','c'};
        String characterString = "abc";
        
        MyInteger me = new MyInteger(number);
        
        System.out.println("Integer: "+me.getValue());
        if(me.isEven()){
            System.out.println("is even (test 1)");
        }
        if(me.isOdd()){
            System.out.println("is odd (test 1)");
        }
        if(me.isPrime()){
            System.out.println("is prime (test 1)");
        }else{
            System.out.println("is not prime (test 1)");
        }
        if(MyInteger.isEven(me)){
            System.out.println("is even (test 2)");
        } else {
        }
        if(MyInteger.isOdd(me)){
            System.out.println("is odd (test 2)");
        }
        if(MyInteger.isPrime(me)){
            System.out.println("is prime (test 2)");
        }else{
            System.out.println("is not prime (test 2)");
        }
        if(me.equals(wrongNumber)){
            System.out.println("Equals test failed "+wrongNumber+" does not equal "+me.getValue());
        }else{
            System.out.println("Equals test passed "+number+" equals "+me.getValue());
        }
        if(me.equals(me)){
            System.out.println("Equals test passed "+me.getValue()+" equals "+me.getValue());
        }else{
            System.out.println("Equals test failed");
        }
        
        System.out.println("Parsing integer of character array, result: "+MyInteger.parseInt(characters));
        System.out.println("Parsing integer of string, result: "+MyInteger.parseInt(characterString));
        
        if(MyInteger.parseInt(characters)==MyInteger.parseInt(characterString)){
            System.out.println("Integer parsing test passed");
        }else{
            System.out.println("Integer parsing test failed");
        }
    }
    
    public static class MyInteger{
        
        // Define data fields
        int value;
        
        // Create constructor
        MyInteger(int value){
            this.value = value;
        }
        
        // Class methods
        int getValue(){
            return value;
        }
        boolean isEven(){
            return (value%2 == 0);
        }
        boolean isOdd(){
            return !(value%2 == 0);
        }
        boolean isPrime(){
            return primeChecker(value);
        }
        static boolean isEven(int a){
            return (a%2 == 0);
        }
        static boolean isOdd(int a){
            return !(a%2 == 0);
        }
        static boolean isPrime(int a){
            return primeChecker(a);
        }
        static boolean isEven(MyInteger a){
            return isEven(a.getValue());
        }
        static boolean isOdd(MyInteger a){
            return isOdd(a.getValue());
        }
        static boolean isPrime(MyInteger a){
            return isPrime(a.getValue());
        }
        boolean equals(int a){
            return (value == a);
        }
        boolean equals(MyInteger a){
            return (value == a.getValue());
        }
        static int parseInt(char[] a){
            int value = 0;
            for(int i = 0;i<a.length;i++){
                value += (int) a[i];
            }
            return value;
        }
        static int parseInt(String a){
            char[] values = new char[a.length()];
            for(int i=0;i<a.length();i++){
                values[i] = a.charAt(i);
            }
            return parseInt(values);
        }
    }
    
    public static boolean primeChecker(int a){
        boolean result = true;
        int half = (int) a/2;
        for(int i=1;i<half;i++){
            if(a%i==0){
                result = false;
            }
        }
        return result;
    }
}
