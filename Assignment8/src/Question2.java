/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question2 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get array dimensions from user
        System.out.print("Enter the number of rows and columns of the array: ");
        int rows = input.nextInt();
        int columns = input.nextInt();
        
        // Decare the array
        double[][] array = new double[rows][columns];
        
        // Map input values to array
        for(int i=0;i<rows;i++){
            System.out.print("Enter values for row #"+i+": ");
            for(int j=0;j<columns;j++){
                array[i][j]=input.nextDouble();
            }
        }
        
        int[] iteration = locateLargest(array);
        if((iteration[0]!=-1)&&(iteration[1]!=-1)){
            System.out.println("The location of the largest element is at "+'('+iteration[0]+","+iteration[1]+')');
        }else{
            System.out.println("Something went wrong, there is no largest value");
        }
    }
    
    // Get largest iteration method
    public static int[] locateLargest(double[][] a){
        int x = -1;
        int y = -1;
        double largest = 0;
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a[i].length;j++){
                if((a[i][j]>largest)||((i==0)&&(j==0))){
                    largest = a[i][j];
                    x = i;
                    y = j;
                }
            }
        }
        int[] returnData = {x,y};
        return returnData;
    }
}
