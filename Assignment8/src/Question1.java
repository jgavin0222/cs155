/**
 *
 * @author JackGavin
 */

// Import the scanner
import java.util.Scanner;

// Initialize the class
public class Question1 {
    
    // Initialize the main method
    public static void main(String[] args){
        
        // Call the scanners constructor
        Scanner input = new Scanner(System.in);
        
        // Get user input
        System.out.print("Enter Matrix 1: ");
        int[][] line1IntArray=new int[3][3];
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                line1IntArray[i][j]=input.nextInt();
            }
        }
        
        // Get user input
        System.out.print("Enter Matrix 2: ");
        int[][] line2IntArray=new int[3][3];
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                line2IntArray[i][j]=input.nextInt();
            }
        }
               
        // Combine the two arrays
        int[][] combineArray = new int[3][3];
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                combineArray[i][j]=line1IntArray[i][j]+line2IntArray[i][j];
            }
        }
        
        // Display the output
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(line1IntArray[i][j]+" ");
            }
            if(i==1){
                System.out.print("  +   ");
            }else{
                System.out.print("      ");
            }
            for(int j=0;j<3;j++){
                System.out.print(line2IntArray[i][j]+" ");
            }
            if(i==1){
                System.out.print("  =   ");
            }else{
                System.out.print("      ");
            }
            for(int j=0;j<3;j++){
                System.out.printf("%-4d",combineArray[i][j]);
            }
            System.out.println();
        }
    }
}
